<?php
/**
 * request
 *
 * @package     request
 * @author      Mohamed Diallo  
 * @copyright   2022 Mohamed Diallo
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: request
 * Version:     1.0.0
 * Author:      Mohamed Diallo
 */




//[request]
function request_func(){

 global $wpdb;

$results = $wpdb->get_results( "SELECT * FROM wp_annuaire"); ?>

  <table class="center" style="margin-left: auto; margin-right: auto; border: 2px solid black ;  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);">
     <tr>
     <th>Id</th>
       <th>Nom Entreprise</th>
       <th>Localisation Entreprise</th>
       <th>Prenom Contact</th>
       <th>Nom Contact</th>
       <th>Mail Contact</th>
     </tr>
    
   <?php  forEach ($results as $res) { 

    $id = $res->id;
    $entreprise = $res ->nom_entreprise;
    $loc = $res->localisation_entreprise;
    $pre_contact = $res->prenom_contact;
    $name = $res->nom_contact;
    $mail= $res->mail_contact;

    
 echo "   <tr>
       <td>$id</td>
       <td>$entreprise</td>
       <td>$loc</td>
       <td>$pre_contact</td>
       <td>$name</td>
       <td>$mail</td>
     </tr> ";

     
  }   ?> 
  </table>

<?php  
  
}
add_shortcode( 'request', 'request_func' );
